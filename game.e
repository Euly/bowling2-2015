note
	description: "{GAME}: see Bowling Kata, slide 3."
	author: "Mattia Monga"

deferred class
	GAME

feature

	roll (pins: INTEGER)
		require
			nonnegative_pins: pins >= 0
			pins_max_10: pins <= 10
			game_ongoing: not ended
		deferred
		end

	ended: BOOLEAN
		deferred
		end

	score: INTEGER
		require
			game_ended: ended
		deferred
		ensure
			nonnegative_score: Result >= 0
			score_max_300: Result <= 300
		end

end
