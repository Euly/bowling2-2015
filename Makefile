export ISE_EIFFEL=/opt/Eiffel_15.08
export ISE_PLATFORM=linux-x86-64
EBIN=$(ISE_EIFFEL)/studio/spec/$(ISE_PLATFORM)/bin
EC=$(EBIN)/ec
EF=$(EBIN)/finish_freezing


bowlinggamedbc: bowlinggamedbc.ecf application.e
	$(EC) -batch -config $<
	( cd EIFGENs/$@/W_code/; $(EF) ) 
	ln EIFGENs/$@/W_code/$@ 

.PHONY: clean

clean:
	rm -f bowlinggamedbc
	rm -rf EIFGENs

